
var state = {};

function setState(s){
    state = s;

    if(state && state.posts){
        state.posts.forEach((post)=>{
            renderPost(post);
            fetchPost(post);
        });
    }
}

function renderPost(post){
    var host = document.getElementById('posts');

    var div = document.createElement('div');
    div.id = post.name;
    div.setAttribute('data-url', post.url);
    
    var heading = document.createElement('h3');
    heading.textContent = post.name;
    div.appendChild(heading);

    var created = document.createElement('label');
    var createdTime = new Date(post.created);
    created.textContent = createdTime.toLocaleDateString();
    div.appendChild(created);

    host.appendChild(div);
}

function fetchPost(post){
    var request = new XMLHttpRequest();
    request.onload = (event)=>{
        onLoadPostRequest(event, post);
    };
    request.open("GET", post.url);
    request.send();
}

function onLoadPostRequest(event, post){
    var request = event.currentTarget;

    if(request.status == 200 && request.readyState == 4){
        renderPostBody(post, request.response);
    }
}

function renderPostBody(post, html){
    var div = document.getElementById(post.name);
    var body = document.createElement('div');
    var result = window.markdownit().render(html);
    body.innerHTML = result;
    div.appendChild(body);
}

function fetchState(){
    var request = new XMLHttpRequest();
    request.onload = onLoadStateRequest;
    request.open("GET", "posts/posts.json");
    request.send();
}

function onLoadStateRequest(event){
    var request = event.currentTarget;

    if(request.status == 200 && request.readyState == 4){
        var state = JSON.parse(request.response);
        setState(state);
    }
}

function start(){
    fetchState();
}