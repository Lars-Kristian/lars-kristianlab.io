@echo off
set "pwd=%cd%"

cd public
rmdir /S /Q posts
call :checkError || goto :eof

cd ..
xcopy "posts" "public/posts" /e /h /i /y
call :checkError || goto :eof


goto :eof

:checkError
if %ERRORLEVEL% neq 0 (
    call :heading "Error %ERRORLEVEL%"
    cd %pwd%
    exit /b %ERRORLEVEL%
)
exit /b

:heading
echo .
echo ------------------------- %~1 -------------------------
echo .
exit /b